package com.sary.assignment

import com.sary.assignment.extensions.toUiModel
import com.sary.assignment.network.io.model.banner.BannerResponse
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class BannerExtensionsTest : BaseTest() {

    private lateinit var bannerResponse: BannerResponse

    @Before
    fun init() {
        bannerResponse = getBannersResponse()
    }

    @Test
    fun `the Banner UI model should be valid`() {
        val banner = bannerResponse.bannerList!!.first()
        val anyPosition = 0
        val uiModel = banner.toUiModel(anyPosition)

        Assert.assertTrue(
            uiModel.imageUrl == banner.image &&
                uiModel.banner == banner &&
                uiModel.position == anyPosition
        )
    }
}
