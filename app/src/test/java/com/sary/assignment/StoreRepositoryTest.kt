package com.sary.assignment

import com.sary.assignment.network.StoreService
import com.sary.assignment.network.io.model.banner.BannerResponse
import com.sary.assignment.network.io.model.catalog.CatalogResponse
import com.sary.assignment.network.io.model.catalog.Other
import com.sary.assignment.repository.StoreRepository
import com.sary.assignment.repository.StoreRepositoryImpl
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnit4::class)
class StoreRepositoryTest {

    lateinit var storeRepository: StoreRepository

    @Mock
    lateinit var storeService: StoreService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        storeRepository = StoreRepositoryImpl(storeService)
    }

    @Test
    fun `get banners test`() {
        val anyInt = 123

        val expectedResponse = BannerResponse(
            bannerList = emptyList(),
            status = true
        )
        runBlocking {
            Mockito.`when`(storeRepository.getBanners(anyInt))
                .thenReturn(
                    Response.success(
                        BannerResponse(emptyList(), true)
                    )
                )
            val response = storeRepository.getBanners(anyInt)
            assertEquals(expectedResponse, response.body())
        }
    }

    @Test
    fun `get catalog test`() {
        val anyId = 123

        val expectedResponse = CatalogResponse(emptyList(), Other(), "anyMessage", true)
        runBlocking {
            Mockito.`when`(storeRepository.getCatalog(anyId))
                .thenReturn(
                    Response.success(
                        CatalogResponse(emptyList(), Other(), "anyMessage", true)
                    )
                )
            val response = storeRepository.getCatalog(anyId)
            assertEquals(expectedResponse, response.body())
        }
    }
}
