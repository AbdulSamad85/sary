package com.sary.assignment

import com.google.gson.Gson
import com.sary.assignment.network.io.model.banner.BannerResponse
import com.sary.assignment.network.io.model.catalog.CatalogResponse
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

abstract class BaseTest {
    private val bannersFilePath = "banners_response.json"
    private val catalogsFilePath = "catalogs_response.json"

    fun getBannersResponse(): BannerResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource(bannersFilePath).toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, BannerResponse::class.java)
    }

    fun getBannersJson(): String {
        val jsonStream: InputStream =
            BannerResponse::class.java.classLoader!!.getResourceAsStream(bannersFilePath)
        return String(jsonStream.readBytes())
    }

    fun getCatalogsResponse(): CatalogResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource(catalogsFilePath).toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, CatalogResponse::class.java)
    }

    fun getCatalogsJson(): String {
        val jsonStream: InputStream =
            CatalogResponse::class.java.classLoader!!.getResourceAsStream(catalogsFilePath)
        return String(jsonStream.readBytes())
    }
}
