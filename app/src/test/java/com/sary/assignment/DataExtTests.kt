package com.sary.assignment

import com.sary.assignment.enums.DataType
import com.sary.assignment.extensions.toUiModel
import com.sary.assignment.network.io.model.catalog.Data
import com.sary.assignment.ui_model.catalogItem.UIBannerItem
import com.sary.assignment.ui_model.catalogItem.UIGroupItem
import com.sary.assignment.ui_model.catalogItem.UISkuItem
import com.sary.assignment.ui_model.catalogItem.UISmartItem
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock

class DataExtTests : BaseTest() {

    lateinit var suit: Data
    @Before
    fun init() {
        suit = mock()
    }

    @Test
    fun `UI types should be BANNER when dataType is BANNER`() {
        val uiModel = suit.toUiModel(DataType.Banner)
        Assert.assertTrue(uiModel is UIBannerItem)
    }

    @Test
    fun `UI types should be SMART when dataType is SMART`() {
        val uiModel = suit.toUiModel(DataType.SMART)
        Assert.assertTrue(uiModel is UISmartItem)
    }

    @Test
    fun `UI types should be SKU when dataType is SKU`() {
        val uiModel = suit.toUiModel(DataType.Sku)
        Assert.assertTrue(uiModel is UISkuItem)
    }

    @Test
    fun `UI types should be GROUP when dataType is GROUP`() {
        val uiModel = suit.toUiModel(DataType.GROUP)
        Assert.assertTrue(uiModel is UIGroupItem)
    }
}
