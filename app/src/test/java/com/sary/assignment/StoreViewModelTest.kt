package com.sary.assignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sary.assignment.common.Constants
import com.sary.assignment.extensions.getOrAwaitValue
import com.sary.assignment.extensions.toUiModel
import com.sary.assignment.repository.StoreRepository
import com.sary.assignment.ui.store.StoreViewModel
import com.sary.assignment.ui_model.UIMainBannerItem
import com.sary.assignment.ui_model.catalog.UICatalog
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class StoreViewModelTest : BaseTest() {
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var storeRepository: StoreRepository
    private lateinit var viewModel: StoreViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = StoreViewModel(storeRepository)
    }

    @Test
    fun `when getBanners() call, should load banners data`() {
        val anyInt = Constants.Api.BASKET_ID
        val list = getBannersResponse().bannerList ?: emptyList()
        val uiBannerList = mutableListOf<UIMainBannerItem>()

        for (i in list.indices) {
            uiBannerList.add(list[i].toUiModel(i))
        }
        runBlocking {
            Mockito.`when`(storeRepository.getBanners(anyInt))
                .thenReturn(Response.success(getBannersResponse()))
            viewModel.getBanners()
            val result = viewModel.mainBannerLiveData.getOrAwaitValue()
            assertEquals(uiBannerList, result)
        }
    }

    @Test
    fun `when getCatalog() call, should load catalog data`() {
        val anyInt = Constants.Api.BASKET_ID
        val list = getCatalogsResponse().result
        val uiCatalogsList = mutableListOf<UICatalog>()
        val gridIndex = 0
        val linearIndex = 4
        val sliderIndex = 1

        list.forEach() {
            uiCatalogsList.add(it.toUiModel())
        }
        val gridUiModel = list[gridIndex].toUiModel()
        val linearUiModel = list[linearIndex].toUiModel()
        val sliderUiModel = list[sliderIndex].toUiModel()

        runBlocking {
            Mockito.`when`(storeRepository.getCatalog(anyInt))
                .thenReturn(Response.success(getCatalogsResponse()))
            viewModel.getCatalog()
            val gridResult = viewModel.gridLiveData.getOrAwaitValue()
            val linearResult = viewModel.linearLiveData.getOrAwaitValue()
            val sliderResult = viewModel.sliderLiveData.getOrAwaitValue()

            assertNotNull(gridResult.dataType == gridUiModel.dataType)
            assertNotNull(linearResult.dataType == linearUiModel.dataType)
            assertNotNull(sliderResult.dataType == sliderUiModel.dataType)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}
