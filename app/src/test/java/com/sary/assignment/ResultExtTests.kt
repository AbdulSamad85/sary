package com.sary.assignment

import com.sary.assignment.enums.CatalogUiType
import com.sary.assignment.extensions.toUiModel
import com.sary.assignment.network.io.model.catalog.CatalogResponse
import com.sary.assignment.network.io.model.catalog.Result
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ResultExtTests : BaseTest() {

    private lateinit var catalogResponse: CatalogResponse
    private val gridIndex = 0
    private val sliderIndex = 1
    private val linearIndex = 2

    @Before
    fun init() {
        catalogResponse = getCatalogsResponse()
    }

    private fun getGridObject(): Result {
        return catalogResponse.result[gridIndex]
    }

    private fun getLinearObject(): Result {
        return catalogResponse.result[linearIndex]
    }

    private fun getSliderObject(): Result {
        return catalogResponse.result[sliderIndex]
    }
    @Test
    fun `when ui_type is GRID then UICatalog type should be GRID`() {
        val result = getGridObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(CatalogUiType.GRID == uiModel.uiType)
    }

    @Test
    fun `when ui_type is SLIDER then UICatalog type should be SLIDER`() {
        val result = getSliderObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(CatalogUiType.SLIDER == uiModel.uiType)
    }

    @Test
    fun `when ui_type is LINEAR then UICatalog type should be LINEAR`() {
        val result = getLinearObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(CatalogUiType.LINEAR == uiModel.uiType)
    }

    @Test
    fun `when ui_type is SLIDER then row count is 1`() {
        val result = getSliderObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.rowCount == 1)
    }

    @Test
    fun `when ui_type is LINEAR then row count is 1`() {
        val result = getLinearObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.rowCount == 1)
    }

    @Test
    fun `when ui_type is GRID then row count is dynamic`() {
        val result = getGridObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.rowCount == result.rowCount)
    }

    @Test
    fun `when ui_type is SLIDER then column count is dynamic`() {
        val result = getSliderObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.columnCount == result.rowCount)
    }

    @Test
    fun `when ui_type is LINEAR then column count is 1`() {
        val result = getLinearObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.columnCount == 1)
    }

    @Test
    fun `when ui_type is GRID then row column is dynamic`() {
        val result = getGridObject()
        val uiModel = result.toUiModel()
        Assert.assertTrue(uiModel.columnCount == result.rowCount)
    }

    @Test
    fun `UICatalogItmCount should be equal to result count`() {
        val expectedSize = getGridObject().data.size
        val actualSize = getGridObject().toUiModel().itemList.size
        Assert.assertEquals(expectedSize, actualSize)
    }
}
