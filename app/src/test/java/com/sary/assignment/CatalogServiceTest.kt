package com.sary.assignment

import com.google.gson.Gson
import com.sary.assignment.network.StoreService
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class CatalogServiceTest : BaseTest() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var storeService: StoreService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        mockWebServer = MockWebServer()
        storeService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build().create(StoreService::class.java)
    }

    @Test
    fun `get banners api test`() {
        val anyId = 123
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody(getBannersJson()))
            val response = storeService.getBanners(anyId)
            val request = mockWebServer.takeRequest()
            val expectedPath =
                "/v2.5.1/baskets/$anyId/banners/"
            assertEquals(expectedPath, request.path)
            assertTrue(response.body() == getBannersResponse())
        }
    }

    @Test
    fun `get catalog api test`() {
        val anyId = 123
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody(getCatalogsJson()))
            val response = storeService.getCatalog(anyId)
            val request = mockWebServer.takeRequest()
            val expectedPath = "/baskets/$anyId/catalog/"
            assertEquals(expectedPath, request.path)
            assertTrue(response.body() == getCatalogsResponse())
        }
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}
