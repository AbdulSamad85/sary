package com.sary.assignment.extensions

import com.sary.assignment.enums.DataType
import com.sary.assignment.network.io.model.catalog.Data
import com.sary.assignment.ui_model.catalogItem.*

/**
 * converts dataType to UICatalogItem
 */
fun Data.toUiModel(dataType: DataType): UICatalogItem {
    val data = this

    return when (dataType) {
        DataType.GROUP -> {
            UIGroupItem(data = data)
        }
        DataType.Banner -> {
            UIBannerItem(data = data)
        }
        DataType.SMART -> {
            UISmartItem(data = data)
        }
        DataType.Sku -> {
            UISkuItem(data = data)
        }
    }
}
