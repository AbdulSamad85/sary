package com.sary.assignment.extensions

import com.sary.assignment.network.io.model.banner.Banner
import com.sary.assignment.ui_model.UIMainBannerItem

/**
 * converts banner to UIBannerItem
 */
fun Banner.toUiModel(position: Int): UIMainBannerItem {
    return UIMainBannerItem(
        banner = this,
        imageUrl = this.image,
        position = position,
    )
}
