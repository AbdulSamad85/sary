package com.sary.assignment.extensions

import com.sary.assignment.enums.CatalogUiType
import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.ui_model.catalog.UICatalog
import com.sary.assignment.ui_model.catalog.UIGrid
import com.sary.assignment.ui_model.catalog.UILinear
import com.sary.assignment.ui_model.catalog.UISlider
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

/**
 * converts result to UICatalog
 */
fun Result.toUiModel(): UICatalog {
    val result = this
    val uiItemList = mutableListOf<UICatalogItem>()
    result.data.forEach {
        uiItemList.add(it.toUiModel(result.dataType))
    }
    return when (result.catalogUiType) {
        CatalogUiType.GRID -> {
            UIGrid(
                result = result,
                itemList = uiItemList,
                rowCount = getRowCount(result),
                columnCount = getColumnCount(result),
            )
        }
        CatalogUiType.LINEAR -> {
            UILinear(
                result = result,
                itemList = uiItemList,
                columnCount = getColumnCount(result),
            )
        }
        CatalogUiType.SLIDER -> {
            UISlider(
                result = result,
                itemList = uiItemList,
            )
        }
    }
}

private fun getRowCount(result: Result): Int {
    return when (result.catalogUiType) {
        CatalogUiType.GRID -> result.rowCount!!
        CatalogUiType.SLIDER -> 1
        CatalogUiType.LINEAR -> 1
    }
}

private fun getColumnCount(result: Result): Int {
    return when (result.catalogUiType) {
        CatalogUiType.GRID -> result.rowCount!!
        CatalogUiType.SLIDER -> result.rowCount!!
        CatalogUiType.LINEAR -> 1
    }
}
