package com.sary.assignment.customViews.banner

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.sary.assignment.ui_model.UIMainBannerItem

class BannerPagerAdapter(
    private val images: List<UIMainBannerItem>,
    private val bannerItemCallbacks: BannersView.BannerItemCallbacks? = null
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val bannerView = BannerItemView(container.context)
        bannerView.updateUi(images[position])
        bannerView.setListener(bannerItemCallbacks)
        container.addView(bannerView)
        return bannerView
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = images.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
