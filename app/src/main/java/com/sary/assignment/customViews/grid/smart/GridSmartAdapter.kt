package com.sary.assignment.customViews.grid.smart

import android.content.Context
import android.view.ViewGroup
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.common.ImageUtils
import com.sary.assignment.customViews.grid.GridItemCallbacks
import com.sary.assignment.databinding.GridSmartItemBinding
import com.sary.assignment.ui_model.catalogItem.UICatalogItem
import com.sary.assignment.ui_model.catalogItem.UISmartItem

class GridSmartAdapter(
    context: Context,
    itemList: List<UICatalogItem>,
    private val listener: GridItemCallbacks?,
) : BaseGridViewAdapter(context, itemList) {

    override fun onBindViewHolder(holder: BaseGridViewHolder, position: Int) {
        holder.populate(getData(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseGridViewHolder {
        val binding = GridSmartItemBinding.inflate(layoutInflater, parent, false)
        return GridSmartViewHolder(binding, listener)
    }

    class GridSmartViewHolder(
        private val binding: GridSmartItemBinding,
        private val listener: GridItemCallbacks?,
    ) : BaseGridViewHolder(binding.root) {

        override fun populate(any: Any, position: Int) {
            val uiItem = any as UISmartItem
            binding.root.setOnClickListener {
                listener?.onItemSelected(position, uiItem.data)
            }
            binding.title.text = uiItem.title
            uiItem.imageUrl?.let {
                ImageUtils.loadImage(
                    binding.root.context,
                    uiItem.imageUrl,
                    binding.image,
                )
            }
        }
    }
}
