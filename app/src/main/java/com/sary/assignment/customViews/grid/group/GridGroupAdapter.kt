package com.sary.assignment.customViews.grid.group

import android.content.Context
import android.view.ViewGroup
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.common.ImageUtils
import com.sary.assignment.customViews.grid.GridItemCallbacks
import com.sary.assignment.databinding.GridGroupItemBinding
import com.sary.assignment.ui_model.catalogItem.UICatalogItem
import com.sary.assignment.ui_model.catalogItem.UIGroupItem

class GridGroupAdapter(
    context: Context,
    itemList: List<UICatalogItem>,
    private val listener: GridItemCallbacks?,
    private val imageWidth: Int,
    private val imageHeight: Int,
    private val useDynamicDimensions: Boolean
) : BaseGridViewAdapter(context, itemList) {

    override fun onBindViewHolder(holder: BaseGridViewHolder, position: Int) {
        holder.populate(getData(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseGridViewHolder {
        val binding = GridGroupItemBinding.inflate(layoutInflater, parent, false)
        return GridGroupViewHolder(binding, listener, imageWidth, imageHeight, useDynamicDimensions)
    }

    class GridGroupViewHolder(
        private val binding: GridGroupItemBinding,
        private val listener: GridItemCallbacks?,
        private val imageWidth: Int,
        private val imageHeight: Int,
        private val useDynamicDimensions: Boolean
    ) : BaseGridViewHolder(binding.root) {

        override fun populate(any: Any, position: Int) {
            val uiGridGroupItem = any as UIGroupItem
            binding.root.setOnClickListener {
                listener?.onItemSelected(position, uiGridGroupItem.data)
            }
            uiGridGroupItem.imageUrl?.let {
                if(useDynamicDimensions) {
                    val params = binding.image.layoutParams
                    params.width = imageWidth
                    params.height = imageHeight
                    binding.image.layoutParams = params
                }

                ImageUtils.loadGridImage(
                    binding.root.context,
                    uiGridGroupItem.imageUrl,
                    binding.image
                )
            }
        }
    }
}
