package com.sary.assignment.customViews.banner

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.sary.assignment.R
import com.sary.assignment.databinding.BannerItemBinding
import com.sary.assignment.ui_model.UIMainBannerItem
import com.squareup.picasso.Picasso

/**
 * Custom view class to show the banner item
 */
class BannerItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : ConstraintLayout(context, attrs) {

    private var callbacks: BannersView.BannerItemCallbacks? = null
    private val imageHeight = context.resources.getDimensionPixelSize(R.dimen.banner_item_height)
    private val imageWidth = context.resources.getDimensionPixelSize(R.dimen.banner_item_width)
    private lateinit var uiMainBanner: UIMainBannerItem
    private val binding = BannerItemBinding.inflate(LayoutInflater.from(context), this, true)

    fun updateUi(uiMainBanner: UIMainBannerItem) {
        this.uiMainBanner = uiMainBanner
        setListeners()
        setImage(uiMainBanner.imageUrl)
    }

    private fun setListeners() {
        binding.image.setOnClickListener {
            callbacks?.onBannerItemClicked(uiMainBanner.banner)
        }
    }

    private fun setImage(url: String) {
        loadImage(url, binding.image)
    }

    private fun loadImage(url: String, imageView: ImageView) {
        Picasso
            .with(context)
            .load(url)
            .resize(imageWidth, imageHeight)
            .error(R.drawable.ic_error)
            .into(imageView)
    }

    fun setListener(bannerItemCallbacks: BannersView.BannerItemCallbacks?) {
        callbacks = bannerItemCallbacks
    }
}
