package com.sary.assignment.customViews.banner

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.sary.assignment.databinding.BannersViewBinding
import com.sary.assignment.extensions.autoScroll
import com.sary.assignment.network.io.model.banner.Banner
import com.sary.assignment.ui_model.UIMainBannerItem
import java.util.concurrent.TimeUnit

/**
 * Custom view class to show the banner item
 */
class BannersView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : ConstraintLayout(context, attrs) {

    interface BannerItemCallbacks {
        fun onBannerItemClicked(banner: Banner)
    }

    private var callbacks: BannerItemCallbacks? = null
    private lateinit var uiMainBannerList: List<UIMainBannerItem>
    private var bannerVisibilityDuration: Long = 0
    private val binding = BannersViewBinding.inflate(LayoutInflater.from(context), this, true)
    private lateinit var adapter: BannerPagerAdapter

    companion object {
        private const val DEFAULT_BANNER_VISIBILITY_INTERVAL = 3L
    }

    fun updateUi(
        uiMainBannerList: List<UIMainBannerItem>,
        bannerVisibilityDuration: Long = TimeUnit.SECONDS.toMillis(
            DEFAULT_BANNER_VISIBILITY_INTERVAL
        )
    ) {
        this.uiMainBannerList = uiMainBannerList
        this.bannerVisibilityDuration = bannerVisibilityDuration
        initBannerViewPager(uiMainBannerList)
    }

    private fun initBannerViewPager(list: List<UIMainBannerItem>) {
        this.adapter = BannerPagerAdapter(list)
        binding.bannerViewPager.adapter = adapter
        /**
         * Start automatic scrolling with an interval of [bannerVisibilityDuration] seconds.
         */
        binding.bannerViewPager.autoScroll(bannerVisibilityDuration)
    }

    fun setCallbacks(bannerItemCallbacks: BannerItemCallbacks?) {
        callbacks = bannerItemCallbacks
    }
}
