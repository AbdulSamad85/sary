package com.sary.assignment.customViews.grid

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.databinding.GridViewBinding
import com.sary.assignment.enums.CatalogUiType
import com.sary.assignment.network.io.model.catalog.Data
import com.sary.assignment.ui_model.catalog.UICatalog
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

/**
 * Custom view class to show the grid of items
 */
open class GridView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : ConstraintLayout(context, attrs), GridItemCallbacks {

    private lateinit var uiCatalogItemList: List<UICatalogItem>
    private val binding = GridViewBinding.inflate(LayoutInflater.from(context), this, true)
    private lateinit var adapter: BaseGridViewAdapter

    fun updateUi(uiCatalog: UICatalog) {
        this.uiCatalogItemList = uiCatalog.itemList
        setTitle(uiCatalog)
        initView(uiCatalog)
    }

    private fun setTitle(uiCatalog: UICatalog) {
        if (uiCatalog.showTitle) {
            binding.title.text = uiCatalog.title
        }
    }

    private fun initView(uiCatalog: UICatalog) {
        this.adapter =
            GridAdapterFactory.getAdapter(context, uiCatalog, this)

        binding.recyclerGridView.layoutManager = getLayoutManager(uiCatalog)
        binding.recyclerGridView.setHasFixedSize(true)
        binding.recyclerGridView.adapter = adapter
    }

    private fun getLayoutManager(uiCatalog: UICatalog): RecyclerView.LayoutManager {
        return when (uiCatalog.uiType) {
            CatalogUiType.GRID -> {
                GridLayoutManager(
                    context, uiCatalog.columnCount ?: 1, RecyclerView.VERTICAL,
                    false
                )
            }
            CatalogUiType.SLIDER -> {
                GridLayoutManager(
                    context, uiCatalog.columnCount ?: 1,
                    RecyclerView.HORIZONTAL, false
                ).apply {
                    this.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            return uiCatalog.columnCount ?: 1
                        }
                    }
                }
            }
            CatalogUiType.LINEAR -> {
                val params = binding.recyclerGridView.layoutParams
                params.width = LayoutParams.WRAP_CONTENT
                params.height = LayoutParams.WRAP_CONTENT
                binding.recyclerGridView.layoutParams = params
                LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            }
        }
    }

    override fun onItemSelected(position: Int, data: Data) {
        Toast.makeText(
            context,
            data.name ?: "Item at position:$position clicked",
            Toast.LENGTH_SHORT
        ).show()
    }
}
