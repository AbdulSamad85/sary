package com.sary.assignment.customViews.grid

import com.sary.assignment.network.io.model.catalog.Data


interface GridItemCallbacks {
    fun onItemSelected(position: Int, data: Data,)
}
