package com.sary.assignment.customViews.grid.banner

import android.content.Context
import android.view.ViewGroup
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.common.ImageUtils
import com.sary.assignment.customViews.grid.GridItemCallbacks
import com.sary.assignment.databinding.GridBannerItemBinding
import com.sary.assignment.ui_model.catalogItem.UIBannerItem
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

class GridBannerAdapter(
    context: Context,
    itemList: List<UICatalogItem>,
    private val listener: GridItemCallbacks?,
    private val imageWidth: Int,
    private val imageHeight: Int
) : BaseGridViewAdapter(context, itemList) {
    override fun onBindViewHolder(holder: BaseGridViewHolder, position: Int) {
        holder.populate(getData(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseGridViewHolder {
        val binding = GridBannerItemBinding.inflate(layoutInflater, parent, false)
        return GridBannerViewHolder(binding, listener, imageWidth, imageHeight)
    }

    class GridBannerViewHolder(
        private val binding: GridBannerItemBinding,
        private val listener: GridItemCallbacks?,
        private val imageWidth: Int,
        private val imageHeight: Int
    ) : BaseGridViewHolder(binding.root) {

        override fun populate(any: Any, position: Int) {
            val uiGridBannerItem = any as UIBannerItem
            binding.root.setOnClickListener {
                listener?.onItemSelected(position, uiGridBannerItem.data)
            }

            uiGridBannerItem.imageUrl?.let {
                val params = binding.image.layoutParams
                params.width = imageWidth
                params.height = imageHeight
                binding.image.layoutParams = params
                ImageUtils.loadImage(
                    binding.root.context,
                    uiGridBannerItem.imageUrl,
                    binding.image,
                )
            }
        }
    }
}
