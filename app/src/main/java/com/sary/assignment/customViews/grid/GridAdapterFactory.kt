package com.sary.assignment.customViews.grid

import android.content.Context
import com.sary.assignment.R
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.customViews.grid.banner.GridBannerAdapter
import com.sary.assignment.customViews.grid.group.GridGroupAdapter
import com.sary.assignment.customViews.grid.sku.GridSkuAdapter
import com.sary.assignment.customViews.grid.smart.GridSmartAdapter
import com.sary.assignment.enums.DataType
import com.sary.assignment.ui_model.catalog.UICatalog

object GridAdapterFactory {
    fun getAdapter(
        context: Context,
        uiCatalog: UICatalog,
        listener: GridItemCallbacks?,
    ): BaseGridViewAdapter {
        return when (uiCatalog.dataType) {

            DataType.GROUP -> {
                val width: Int =
                    context.resources.displayMetrics.widthPixels / (uiCatalog.columnCount ?: 1)
                val height: Int = width
                val useDynamicDimensions = uiCatalog.columnCount != 1
                GridGroupAdapter(
                    context = context,
                    itemList = uiCatalog.itemList,
                    listener = listener,
                    width,
                    height,
                    useDynamicDimensions
                )
            }
            DataType.SMART -> GridSmartAdapter(
                context = context,
                itemList = uiCatalog.itemList,
                listener = listener
            )
            DataType.Banner -> {
                val width: Int = context.resources.displayMetrics.widthPixels
                val height: Int =
                    context.resources.getDimension(R.dimen.grid_banner_image_height).toInt()
                GridBannerAdapter(
                    context = context,
                    itemList = uiCatalog.itemList,
                    listener = listener,
                    width / uiCatalog.rowCount,
                    imageHeight = height
                )
            }
            DataType.Sku -> GridSkuAdapter(
                context = context,
                itemList = uiCatalog.itemList,
                listener = listener
            )
        }
    }
}
