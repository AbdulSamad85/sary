package com.sary.assignment.customViews.grid.sku

import android.content.Context
import android.view.ViewGroup
import com.sary.assignment.abstraction.BaseGridViewAdapter
import com.sary.assignment.common.ImageUtils
import com.sary.assignment.customViews.grid.GridItemCallbacks
import com.sary.assignment.databinding.GridSkuItemBinding
import com.sary.assignment.ui_model.catalogItem.UICatalogItem
import com.sary.assignment.ui_model.catalogItem.UISkuItem

class GridSkuAdapter(
    context: Context,
    itemList: List<UICatalogItem>,
    private val listener: GridItemCallbacks?,
) : BaseGridViewAdapter(context, itemList) {

    override fun onBindViewHolder(holder: BaseGridViewHolder, position: Int) {
        holder.populate(getData(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseGridViewHolder {
        val binding = GridSkuItemBinding.inflate(layoutInflater, parent, false)
        return GridSkuViewHolder(binding, listener)
    }

    class GridSkuViewHolder(
        private val binding: GridSkuItemBinding,
        private val listener: GridItemCallbacks?,
    ) : BaseGridViewHolder(binding.root) {

        override fun populate(any: Any, position: Int) {
            val uiItem = any as UISkuItem
            binding.root.setOnClickListener {
                listener?.onItemSelected(position, uiItem.data)
            }
            uiItem.imageUrl?.let {
                ImageUtils.loadImage(
                    binding.root.context,
                    uiItem.imageUrl,
                    binding.image,
                )
            }
        }
    }
}
