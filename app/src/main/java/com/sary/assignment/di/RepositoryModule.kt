package com.sary.assignment.di

import com.sary.assignment.network.StoreService
import com.sary.assignment.repository.StoreRepository
import com.sary.assignment.repository.StoreRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {
    @Singleton
    @Provides
    fun provideStoreRepository(storeService: StoreService): StoreRepository {
        return StoreRepositoryImpl(storeService)
    }
}
