package com.sary.assignment.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sary.assignment.common.Constants
import com.sary.assignment.network.HttpInterceptor
import com.sary.assignment.network.StoreService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RetrofitModule {
    companion object {
        private const val MS_TIMEOUT = 30L
    }

    @Singleton
    @Provides
    fun providesGsonBuilder(): Gson {
        return GsonBuilder().create()
    }

    @Singleton
    @Provides
    fun provideLoggingInterceptor(): Interceptor {
        return HttpInterceptor()
    }

    @Singleton
    @Provides
    fun providesHttpClient(interceptor: HttpInterceptor): OkHttpClient {

        return OkHttpClient.Builder()
            .callTimeout(MS_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(MS_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(MS_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(MS_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()
    }

    @Singleton
    @Provides
    fun providesRetrofit(gson: Gson, client: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(Constants.Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
    }

    @Singleton
    @Provides
    fun providesStoreService(retrofitBuilder: Retrofit.Builder): StoreService {
        return retrofitBuilder
            .build()
            .create(StoreService::class.java)
    }
}
