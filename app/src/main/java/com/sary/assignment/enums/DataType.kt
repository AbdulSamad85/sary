package com.sary.assignment.enums

import com.google.gson.annotations.SerializedName

enum class DataType(val value: String) {
    @SerializedName("smart")
    SMART("smart"),
    @SerializedName("group")
    GROUP("group"),
    @SerializedName("banner")
    Banner("banner"),
    @SerializedName("sku")
    Sku("sku")
}
