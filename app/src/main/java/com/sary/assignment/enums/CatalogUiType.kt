package com.sary.assignment.enums

import com.google.gson.annotations.SerializedName

enum class CatalogUiType(val value: String) {
    @SerializedName("grid")
    GRID("grid"),
    @SerializedName("linear")
    LINEAR("linear"),
    @SerializedName("slider")
    SLIDER("slider");
}
