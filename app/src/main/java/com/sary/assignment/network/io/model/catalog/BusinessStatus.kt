package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class BusinessStatus(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("title") var title: String? = null
) : Parcelable
