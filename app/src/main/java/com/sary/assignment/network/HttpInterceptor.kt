package com.sary.assignment.network

import android.util.Log
import com.sary.assignment.BuildConfig
import com.sary.assignment.common.Constants
import okhttp3.*
import okio.Buffer
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import javax.inject.Inject

/**
 * To provide detailed logging of API calls
 */
class HttpInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request().newBuilder()
            .addHeader(Constants.Api.Header.KEY_DEVICE_TYPE, Constants.Api.Header.VALUE_DEVICE_TYPE)
            .addHeader(Constants.Api.Header.KEY_APP_VERSION, Constants.Api.Header.VALUE_APP_VERSION)
            .addHeader(
                Constants.Api.Header.KEY_ACCEPT_LANGUAGE,
                Constants.Api.Header.VALUE_ACCEPT_LANGUAGE
            )
            .addHeader(Constants.Api.Header.KEY_PLATFORM, Constants.Api.Header.VALUE_PLATFORM)
            .addHeader(
                Constants.Api.Header.KEY_AUTHORIZATION,
                Constants.Api.Header.VALUE_AUTHORIZATION
            )
            .build()
        val response: Response = chain.proceed(request)
        val responseBody: ResponseBody? = response.body
        val source = responseBody?.source()
        source?.request(Long.MAX_VALUE) // Buffer the entire body.
        val buffer: Buffer? = source?.buffer
        // todo create centralized logger
        if (BuildConfig.DEBUG) {
            Log.d(BuildConfig.APPLICATION_ID, "call ==> " + request.url)
            Log.d(
                BuildConfig.APPLICATION_ID,
                "response ==> " + buffer?.clone()
                    ?.readString(Charset.forName(StandardCharsets.UTF_8.name())).toString()
            )
        }
        return response
    }
}
