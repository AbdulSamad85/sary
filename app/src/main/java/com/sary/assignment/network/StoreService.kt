package com.sary.assignment.network

import com.sary.assignment.common.Constants
import com.sary.assignment.network.io.model.banner.BannerResponse
import com.sary.assignment.network.io.model.catalog.CatalogResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface StoreService {

    @GET(Constants.Api.BANNER_URL)
    suspend fun getBanners(
        @Path("basket_id") basketId: Int
    ): Response<BannerResponse>

    @GET(Constants.Api.CATALOG_URL)
    suspend fun getCatalog(
        @Path("basket_id") basketId: Int
    ): Response<CatalogResponse>
}
