package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CatalogResponse(
    @SerializedName("result") var result: List<Result>,
    @SerializedName("other") var other: Other?,
    @SerializedName("message") var message: String?,
    @SerializedName("status") var status: Boolean?
) : Parcelable
