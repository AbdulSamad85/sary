package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Other(
    @SerializedName("show_special_order_view") var showSpecialOrderView: Boolean? = null,
    @SerializedName("uncompleted_profile_settings") var uncompletedProfileSettings: UncompletedProfileSettings? = UncompletedProfileSettings(),
    @SerializedName("business_status") var businessStatus: BusinessStatus? = BusinessStatus()
) : Parcelable
