package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sary.assignment.enums.CatalogUiType
import com.sary.assignment.enums.DataType
import kotlinx.parcelize.Parcelize

@Parcelize
data class Result(
    @SerializedName("id") val id: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("data") val data: ArrayList<Data>,
    @SerializedName("data_type") val dataType: DataType,
    @SerializedName("show_title") val showTitle: Boolean?,
    @SerializedName("ui_type") val catalogUiType: CatalogUiType,
    @SerializedName("row_count") val rowCount: Int?
) : Parcelable
