package com.sary.assignment.network.io.model.banner

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class BannerResponse(
    @SerializedName("result") val bannerList: List<Banner>?,
    @SerializedName("status") val status: Boolean
) : Parcelable
