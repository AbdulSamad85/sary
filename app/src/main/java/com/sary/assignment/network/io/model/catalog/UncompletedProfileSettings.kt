package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class UncompletedProfileSettings(
    @SerializedName("show_tag") var showTag: Boolean? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("is_completed_profile") var isCompletedProfile: Boolean? = null
) : Parcelable
