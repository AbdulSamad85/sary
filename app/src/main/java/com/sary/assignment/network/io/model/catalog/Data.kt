package com.sary.assignment.network.io.model.catalog

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Data(
    @SerializedName("group_id") var groupId: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("empty_content_image") var emptyContentImage: String? = null,
    @SerializedName("empty_content_message") var emptyContentMessage: String? = null,
    @SerializedName("has_data") var hasData: Boolean? = null,
    @SerializedName("show_unavailable_items") var showUnavailableItems: Boolean? = null,
    @SerializedName("show_in_brochure_link") var showInBrochureLink: Boolean? = null
) : Parcelable
