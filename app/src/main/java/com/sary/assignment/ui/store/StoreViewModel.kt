package com.sary.assignment.ui.store

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sary.assignment.common.Constants
import com.sary.assignment.enums.DataType
import com.sary.assignment.extensions.toUiModel
import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.repository.StoreRepository
import com.sary.assignment.ui_model.UIMainBannerItem
import com.sary.assignment.ui_model.catalog.UICatalog
import com.sary.assignment.ui_model.catalog.UIGrid
import com.sary.assignment.ui_model.catalog.UILinear
import com.sary.assignment.ui_model.catalog.UISlider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreViewModel
@Inject constructor(
    private val storeRepository: StoreRepository
) : ViewModel() {

    val mainBannerLiveData: LiveData<List<UIMainBannerItem>> get() = _mainBannerLiveData
    private val _mainBannerLiveData: MutableLiveData<List<UIMainBannerItem>> = MutableLiveData()

    val gridLiveData: LiveData<UIGrid> get() = _gridLiveData
    private val _gridLiveData: MutableLiveData<UIGrid> = MutableLiveData()

    val linearLiveData: LiveData<UILinear> get() = _linearLiveData
    private val _linearLiveData: MutableLiveData<UILinear> = MutableLiveData()

    val sliderLiveData: LiveData<UISlider> get() = _sliderLiveData
    private val _sliderLiveData: MutableLiveData<UISlider> = MutableLiveData()

    val progressBarLiveData: LiveData<Boolean> get() = _progressBarLiveData
    private val _progressBarLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val toastLiveData: LiveData<String> get() = _toastLiveData
    private val _toastLiveData: MutableLiveData<String> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError("Exception: : ${throwable.localizedMessage}")
    }

    fun getBanners() {
        viewModelScope.launch(exceptionHandler) {
            showProgress(true)
            val response = storeRepository.getBanners(Constants.Api.BASKET_ID)
            showProgress(false)
            if (response.isSuccessful) {
                if (response.body()?.status == true) {
                    response.body()?.bannerList?.let { it ->
                        val uiBannerList = mutableListOf<UIMainBannerItem>()
                        for (i in it.indices) {
                            uiBannerList.add(it[i].toUiModel(i))
                        }
                        _mainBannerLiveData.postValue(uiBannerList)
                    }
                }
            } else {
                onError("Error : ${response.errorBody()} ")
            }
        }
    }

    fun getCatalog() {
        viewModelScope.launch(exceptionHandler) {
            showProgress(true)
            val response = storeRepository.getCatalog(Constants.Api.BASKET_ID)
            if (response.isSuccessful) {
                if (response.body()?.status == true) {
                    response.body()?.result?.let { resultList ->
                        createUiModels(resultList)
                    }
                    showProgress(false)
                } else {
                    onError("Error : ${response.body()?.message} ")
                }
            } else {
                onError("Error : ${response.message()} ")
            }
        }
    }

    private fun createUiModels(resultList: List<Result>) {
        resultList.forEach { result ->
            // ignoring SKU data type as there is no documentation about it
            if (result.dataType != DataType.Sku) {
                postData(result.toUiModel())
            }
        }
    }

    private fun postData(uiModel: UICatalog) {
        when (uiModel) {
            is UIGrid -> _gridLiveData.value = uiModel
            is UILinear -> _linearLiveData.value = uiModel
            is UISlider -> _sliderLiveData.value = uiModel
        }
    }

    private fun showProgress(show: Boolean) {
        _progressBarLiveData.postValue(show)
    }

    private fun onError(message: String) {
        showProgress(false)
        showError(message)
    }

    private fun showError(message: String) {
        _toastLiveData.postValue(message)
    }
}
