package com.sary.assignment.ui.store

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import javax.inject.Inject

class StoreFragmentFactory
@Inject
constructor() : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        return when (className) {
            StoreFragment::class.java.name -> {
                StoreFragment()
            }
            else -> return super.instantiate(classLoader, className)
        }
    }
}
