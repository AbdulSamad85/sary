package com.sary.assignment.ui.store

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.sary.assignment.R
import com.sary.assignment.abstraction.BaseFragment
import com.sary.assignment.customViews.banner.BannersView
import com.sary.assignment.customViews.grid.GridView
import com.sary.assignment.databinding.FragmentStoreBinding
import com.sary.assignment.extensions.observeNonNull
import com.sary.assignment.extensions.show
import com.sary.assignment.network.io.model.banner.Banner
import com.sary.assignment.ui_model.UIMainBannerItem
import com.sary.assignment.ui_model.catalog.UICatalog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StoreFragment : BaseFragment(R.layout.fragment_store), BannersView.BannerItemCallbacks {

    private val viewModel: StoreViewModel by viewModels()
    private lateinit var binding: FragmentStoreBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentStoreBinding.bind(view)
        initializeResources()
        observeData()
        loadBanners()
        loadCatalog()
    }

    private fun initializeResources() {
        binding.bannersView.setCallbacks(this)
    }

    private fun observeData() {
        viewModel.run {
            mainBannerLiveData.observeNonNull(this@StoreFragment) {
                updateBannerView(it)
            }
            progressBarLiveData.observe(viewLifecycleOwner) {
                showProgressBar(it)
            }
            toastLiveData.observe(viewLifecycleOwner) {
                showToast(it)
            }
            gridLiveData.observeNonNull(this@StoreFragment) {
                addView(getGridView(it))
            }
            linearLiveData.observeNonNull(this@StoreFragment) {
                addView(getGridView(it))
            }
            sliderLiveData.observeNonNull(this@StoreFragment) {
                addView(getGridView(it))
            }
        }
    }

    private fun getGridView(uiCatalog: UICatalog): View {
        val gridView = GridView(requireContext())
        gridView.layoutParams = ViewGroup.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        gridView.updateUi(uiCatalog)
        return gridView
    }

    private fun addView(view: View) {
        binding.dynamicViewContainer.addView(view)
    }

    private fun updateBannerView(bannersList: List<UIMainBannerItem>) {
        binding.bannersView.updateUi(bannersList)
    }

    private fun showProgressBar(show: Boolean) {
        binding.progressBar.show(show)
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    private fun loadBanners() {
        viewModel.getBanners()
    }

    private fun loadCatalog() {
        viewModel.getCatalog()
    }

    override fun onBannerItemClicked(banner: Banner) {
        Toast.makeText(requireContext(), banner.image, Toast.LENGTH_SHORT).show()
    }
}
