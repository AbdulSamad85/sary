package com.sary.assignment.repository

import com.sary.assignment.network.StoreService

class StoreRepositoryImpl
constructor(private val storeService: StoreService) : StoreRepository {

    override suspend fun getBanners(basketId: Int) =
        storeService.getBanners(basketId)

    override suspend fun getCatalog(basketId: Int) =
        storeService.getCatalog(basketId)
}
