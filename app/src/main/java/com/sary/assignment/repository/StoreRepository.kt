package com.sary.assignment.repository

import com.sary.assignment.network.io.model.banner.BannerResponse
import com.sary.assignment.network.io.model.catalog.CatalogResponse
import retrofit2.Response

interface StoreRepository {
    suspend fun getBanners(basketId: Int): Response<BannerResponse>
    suspend fun getCatalog(basketId: Int): Response<CatalogResponse>
}
