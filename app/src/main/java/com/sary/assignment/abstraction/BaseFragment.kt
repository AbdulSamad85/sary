package com.sary.assignment.abstraction

import androidx.fragment.app.Fragment

abstract class BaseFragment(layoutResourceId: Int) : Fragment(layoutResourceId) {
    // keeping for any future usage
}
