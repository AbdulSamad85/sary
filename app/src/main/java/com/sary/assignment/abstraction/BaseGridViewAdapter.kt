package com.sary.assignment.abstraction

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

abstract class BaseGridViewAdapter protected constructor(
    context: Context,
    var dataList: List<UICatalogItem>? = null,
) :
    RecyclerView.Adapter<BaseGridViewAdapter.BaseGridViewHolder>() {

    protected val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    protected fun getData(index: Int): Any {
        return dataList!![index]
    }

    fun setData(dataList: List<UICatalogItem>?) {
        this.dataList = dataList
    }

    override fun getItemCount(): Int {
        return if (dataList != null) dataList!!.size else 0
    }

    abstract class BaseGridViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        abstract fun populate(any: Any, position: Int)
    }
}
