package com.sary.assignment.ui_model

import android.os.Parcelable
import com.sary.assignment.network.io.model.banner.Banner
import kotlinx.parcelize.Parcelize

/**
 * UIModel for custom Banner view
 */
@Parcelize
data class UIMainBannerItem(
    val banner: Banner,
    val imageUrl: String,
    val position: Int,
) : Parcelable
