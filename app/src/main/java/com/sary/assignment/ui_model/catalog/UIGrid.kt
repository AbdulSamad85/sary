package com.sary.assignment.ui_model.catalog

import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

class UIGrid(
    override val result: Result,
    override val itemList: MutableList<UICatalogItem>, // uiCatalogItem
    override val columnCount: Int,
    override val rowCount: Int,
) : UICatalog(result = result, itemList = itemList, rowCount = rowCount, columnCount = columnCount)
