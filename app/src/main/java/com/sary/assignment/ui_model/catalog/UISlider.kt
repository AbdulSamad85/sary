package com.sary.assignment.ui_model.catalog

import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

class UISlider(
    override val result: Result,
    override val itemList: MutableList<UICatalogItem>,
) : UICatalog(result = result, itemList = itemList, rowCount = 1)
