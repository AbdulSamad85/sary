package com.sary.assignment.ui_model.catalogItem

import android.os.Parcelable
import com.sary.assignment.network.io.model.catalog.Data
import kotlinx.parcelize.Parcelize

@Parcelize
class UIBannerItem(
    override val data: Data,
) : UICatalogItem(data, data.image), Parcelable
