package com.sary.assignment.ui_model.catalogItem

import com.sary.assignment.network.io.model.catalog.Data

/**
 * UIModel for custom Banner view
 */

abstract class UICatalogItem(
    open val data: Data,
    val imageUrl: String?,
)
