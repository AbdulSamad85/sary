package com.sary.assignment.ui_model.catalogItem

import android.os.Parcelable
import com.sary.assignment.network.io.model.catalog.Data
import kotlinx.parcelize.Parcelize

@Parcelize
class UISmartItem(
    override val data: Data,
    val title: String = data.name ?: ""
) : UICatalogItem(data, data.image), Parcelable
