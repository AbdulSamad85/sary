package com.sary.assignment.ui_model.catalog

import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

class UILinear(
    override val result: Result,
    override val itemList: MutableList<UICatalogItem>,
    override val columnCount: Int
) : UICatalog(result = result, columnCount = columnCount, itemList = itemList)
