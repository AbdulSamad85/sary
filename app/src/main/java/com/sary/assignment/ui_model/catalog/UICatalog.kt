package com.sary.assignment.ui_model.catalog

import com.sary.assignment.enums.CatalogUiType
import com.sary.assignment.enums.DataType
import com.sary.assignment.network.io.model.catalog.Result
import com.sary.assignment.ui_model.catalogItem.UICatalogItem

abstract class UICatalog(
    open val result: Result,
    val title: String? = result.title,
    val dataType: DataType = result.dataType,
    val uiType: CatalogUiType = result.catalogUiType,
    open val itemList: MutableList<UICatalogItem>, // uiCatalogItem
    val showTitle: Boolean = result.showTitle ?: false && title?.isNotEmpty() ?: false,
    open val columnCount: Int? = 1,
    open val rowCount: Int = 1,
)
