package com.sary.assignment.common

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso

object ImageUtils {
    fun loadImage(
        context: Context,
        url: String,
        imageView: ImageView,
    ) {
        Picasso.with(context)
            .load(url)
            .fit()
            .into(imageView)
    }

    fun loadGridImage(
        context: Context,
        url: String,
        imageView: ImageView,
    ) {
        Picasso.with(context)
            .load(url)
            .into(imageView)
    }
}
