package com.sary.assignment.common

class Constants {
    sealed class Api {
        class Header {
            companion object {
                const val KEY_DEVICE_TYPE = "Device-Type"
                const val KEY_APP_VERSION = "App-Version"
                const val KEY_ACCEPT_LANGUAGE = "Accept-Language"
                const val KEY_PLATFORM = "Platform"
                const val KEY_AUTHORIZATION = "Authorization"

                const val VALUE_DEVICE_TYPE = "android"
                const val VALUE_APP_VERSION = "5.5.0.0.0" // can be inserted from gradle
                const val VALUE_ACCEPT_LANGUAGE = "en"
                const val VALUE_PLATFORM = "FLAGSHIP" // can be inserted from gradle variable
                const val VALUE_AUTHORIZATION =
                    "token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjgxNTEyLCJ1c2VyX3Bob25lIjoiOTY2NTkxMTIyMzM0In0.phRQP0e5yQrCVfZiN4YlkI8NhXRyqa1fGRx5rvrEv0o"
            }
        }

        companion object {
            const val BASKET_ID = 325514
            private const val API_VERSION = "v2.5.1"
            const val BASE_URL = "https://staging.sary.to/api/"
            const val BANNER_URL = "$API_VERSION/baskets/{basket_id}/banners/"
            const val CATALOG_URL = "baskets/{basket_id}/catalog/"
        }
    }
}
